-module(db_server).

-export ([init/0, loop/0]).

init() -> register(server, spawn(db_server, loop, [])).

loop()   -> loop([]).
loop(Db) ->
    receive
        {stop} -> io:fwrite("Server closed"), ok;

        {get, Pid, Key} ->
            Pid ! get(Db, Key),
            loop(Db);

        {set, Pid, Key, Value} -> loop( set(Db, Key, Value) );

        {delete, Pid, Key} -> loop( delete(Db, Key) );

        _ -> loop(Db)
    end.

get([], _) -> nil;
get([{DbKey, DbValue}|T], Key) ->
    case Key of
        DbKey -> {result, DbValue};
        _     -> get(T, Key)
    end.

set(Db, Key, Value) -> [{Key, Value} | Db].

delete(Db, Key) -> delete(Db, Key, []).

delete([],                     _, NewDb) -> NewDb;
delete([{DbKey, DbValue}|T], Key, NewDb) ->
    case Key of
        DbKey -> delete( [], Key, lists:append([T, NewDb]) );
        _     -> delete( T,  Key, [{DbKey, DbValue}|NewDb] )
    end.
